import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {AngularFireDatabase} from 'angularfire2/database';
@Injectable()
export class UsersService {
  http:Http;
getUsers(){
return this.http.get('http://localhost/angular/slim/users');
}
  postUser(data){
    let token = localStorage.getItem('token');
    //המרת גייסון למפתח וערך
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded',
       
      })  }
      let params = new HttpParams().append('name',data.name).append('phonenumber',data.phonenumber);
      return this.http.post('http://localhost/angular/slim/users', params.toString(), options);
}
    getUsersFire(){
      return this.db.list('/users').valueChanges();
    }


deleteUser(key){
  return this.http.delete('http://localhost/angular/slim/users/'+ key);
}

//Get one user
getUser(id){
  return this.http.get('http://localhost/angular/slim/users/'+ id);
}


//Put
/*putUser(data,key){
 let options = {
   headers: new Headers({
     'content-type':'application/x-www-form-urlencoded'
   })
 }
 //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
 let params = new HttpParams().append('name',data.name).append('phonenumber',data.phonenumber);
 return this.http.put('http://localhost/angular/slim/users/'+ key,params.toString(), options);
}*/
//this works
putUser(id,user){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',user.name).append('phonenumber',user.phonenumber);
    return this.http.put('http://localhost/angular/slim/users/'+id, params.toString(), options);      
  }



  

  constructor(http:Http, private db:AngularFireDatabase) { 
    this.http = http;
    /*let service = new UsersService();
    this.users = service.getUsers(); */ }

}
