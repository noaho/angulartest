import { Component, OnInit } from '@angular/core';
import { UsersService } from "../users.service";
import { FormGroup , FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
 usersform = new FormGroup({
    name: new FormControl('',Validators.required),
    phonenumber: new FormControl('',Validators.required)
  });


  usersformupdate = new FormGroup({
    name: new FormControl('',Validators.required),
    phonenumber: new FormControl('',Validators.required)
  });

  showSlim:Boolean = true;
  users;
  usersKeys = [];
  updates = [];
  lastOpenedToUpdate;
  fusers;

  sendData(){
    if(this.usersform.invalid) return;
    this.service.postUser(this.usersform.value).subscribe(response =>{
      console.log(response);
      this.service.getUsers().subscribe(response => {
        this.users =  response.json();
        this.usersKeys = Object.keys(this.users);
      });      
    });
  }

  deleteUser(id){
      console.log(id);
      this.service.deleteUser(id).subscribe(response=>{
        this.service.getUsers().subscribe(response=>{
          this.users =  response.json();
          this.usersKeys = Object.keys(this.users);         
        })
      })
  }

  showUpdate(key){
    if(this.updates[key]){
      this.updates[key] = false;
    }else{
      if(this.lastOpenedToUpdate){
        this.updates[this.lastOpenedToUpdate] = false;
      }
      this.updates[key] = true;
      this.usersformupdate.get('name').setValue(this.users[key].name);
      this.usersformupdate.get('phonenumber').setValue(this.users[key].phonenumber);
      this.lastOpenedToUpdate = key;      
    } 
  }

  updateUser(id){
    if(this.usersformupdate.invalid) return;
    this.service.putUser(id,this.usersformupdate.value).subscribe(response =>{
      console.log(response);
      this.service.getUsers().subscribe(response => {
        this.users =  response.json();
        this.usersKeys = Object.keys(this.users);
      });      
    });    
  }
  constructor(private service:UsersService) {
    /*service.getUsers().subscribe(response=>{
      //console.log(response))
        this.users = response.json();
        this.usersKeys = Object.keys(this.users);
    //let service = new UsersService();*/
      
   
 
  }
  /*deleteUser(key){
   console.log(key);
   let index = this.usersKeys.indexOf(key);
   this.usersKeys.splice(index,1);
   //delete from the server
   this.service.deleteUser(key).subscribe(response=>console.log(response));    
 }

 updateUser(id){
    this.service.getUser(id).subscribe(response=>{
    this.user = response.json();      
   });  
 }

 pasemisticAdd(){
  this.service.getUsers().subscribe(response=>{
        this.users = response.json();
        this.usersKeys = Object.keys(this.users);      
    });      
  }  */
  
  ngOnInit() {
     this.service.getUsers().subscribe(response => {
      this.users =  response.json();
      this.usersKeys = Object.keys(this.users);
    });
    this.service.getUsersFire().subscribe(fusers =>{
      this.fusers = fusers;
      console.log(this.fusers);
    }); 
  }

}
