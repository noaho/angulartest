import { Component, OnInit , Output , EventEmitter } from '@angular/core';
import {FormGroup , FormControl, FormBuilder, Validators} from '@angular/forms';
import { UsersService } from "../../users.service";

@Component({
  selector: 'add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css']
})
export class AddFormComponent implements OnInit {
  @Output() addUser:EventEmitter <any> = new EventEmitter <any>();
  @Output() addUserPs:EventEmitter <any> = new EventEmitter <any>();

  service:UsersService;
  userform = new FormGroup({
  name:new FormControl(),
  phonenumber:new FormControl(),
 })
  sendData(){
  console.log(this.userform.value);
  this.service.postUser(this.userform.value).subscribe(
     response => {
       console.log(response.json())
  })}
  
  constructor(service:UsersService, private formBuilder:FormBuilder) {
    this.service = service;
   }

  ngOnInit() {
    	this.userform = this.formBuilder.group({
      name:  [null, [Validators.required]],
	    phonenumber: [null, Validators.required],
   });	
  }

}
