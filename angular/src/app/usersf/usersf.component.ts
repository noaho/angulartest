import { Component, OnInit } from '@angular/core';
import { UsersService } from "../users.service";

@Component({
  selector: 'app-usersf',
  templateUrl: './usersf.component.html',
  styleUrls: ['./usersf.component.css']
})
export class UsersfComponent implements OnInit {
users;
  constructor(private service:UsersService) { }

  ngOnInit() {
    this.service.getUsersFire().subscribe(respones=>{
      console.log(respones);
      this.users = respones;
    })
  }

}
